var _getToAptrive = (function(w) {

	var APTRIVE_OFFICE = "Northern Virginia, Tyson's Corner, VA";

	var navigatorOptions = {
		enableHighAccuracy: true,
		timeout: 5000,
		maximumAge: 0
	};

	var Route = function(origin, destination, travelMode) {
		this.origin = origin;
		this.destination = destination;
		this.travelMode = travelMode;
	};

	/* Get _getToAptrive elements */
	var Error = document.getElementById("_getToAptrive_Error");

	var Loading = document.getElementById("_getToAptrive_Loading");


	w.initMap = function () {

	// Initialize Google Maps Directions Service & Display
	var directionsDisplay = new google.maps.DirectionsRenderer();
	var directionsService = new google.maps.DirectionsService();

	/* Add event listeners to every _gtCT button */
	var _gtCT_buttons = document.getElementsByClassName("_getToAptrive");
	for(var i = 0; i < _gtCT_buttons.length; i++) {
		_gtCT_buttons[i].addEventListener("click", function() {

			var that = this;
			Loading.style.display = "block";	

			navigator.geolocation.getCurrentPosition(function(position) { 
				Loading.style.display = "none";

				/* Parse attributes to construct Route object */
				var origin = position.coords.latitude + "," + position.coords.longitude;
				var travelMode = that.children[0].value;
				var newRoute = new Route(origin, APTRIVE_OFFICE, travelMode);

				/* Binds Directions Display Panel to _getToAptrive placeholder */
				directionsDisplay.setPanel(document.getElementById("_getToAptrive_Directions"));

				/* Calculates and display route */
				calculateAndDisplayRoute(directionsService, directionsDisplay, newRoute);

			}, function() {
				Loading.style.display = "none";
				Error.innerHTML = "Sorry, we couldn't locate you. :(";

			}, navigatorOptions);

		});
	} 
	


	function calculateAndDisplayRoute(directionsService, directionsDisplay, route) {
		directionsService.route(route, function(response, status) {
			if (status === google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
			} else {
				Error.innerHTML = status + "Request for directions failed. :(";
			}
		});
	}


}

}(window));



